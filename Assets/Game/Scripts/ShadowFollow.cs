﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShadowFollow : MonoBehaviour
{
    [Header("Player's follow")]
    public GameObject player;

    public NavMeshAgent agent;
    public float minDistance;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        agent.speed = speed;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) > minDistance)
        {
            agent.SetDestination(player.transform.position);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //TODO Ombre de la caméra en plus puissant
        }
    }
}
