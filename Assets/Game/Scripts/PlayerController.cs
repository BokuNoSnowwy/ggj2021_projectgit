﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    
    private CharacterController _charaController;
    
    public float gravity = -9.81f;
    [SerializeField] private float fallMultiplier = 2f;
    [SerializeField] private float rotationSensivity = 5;
    
    //Walking
    public float speed = 6;
    [SerializeField] private float slopeSpeed;
    [SerializeField] private float normalSpeed;
    
    //Time between each footsep sound
    [SerializeField] private float soundTimer;
    private float sTimer;
    
    //Axis Move
    [SerializeField]private float horizontal;
    [SerializeField]private float vertical;

    private Vector3 _velocity;
    private Quaternion _targetRotation;
    
    [Header("Slope Detection")] 
    [SerializeField] private Transform slopeDetector;
    [SerializeField] private LayerMask layerTerrain;

    [Header("Check Ice")] 
    [SerializeField] private bool isOnIce;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float checkDistance;
    [SerializeField] private LayerMask iceLayer;

    [SerializeField] private Vector3 stockedVelocity;
    [SerializeField] private float timer = 1f;
    [SerializeField] private float timerMax = 1f;
    [SerializeField] private bool islippering;

    [Header("Camera")]  
    [SerializeField] private CinemachineVirtualCamera cinemachineCamera;
    [SerializeField] private float offsetSpeedMultiplier;
    private CinemachineTransposer _transposer;
    
    [Header("Animations")]
    [SerializeField] private Animator _animator;
    public int velocityHash;
    
    // Start is called before the first frame update
    void Start()
    {
        _charaController = GetComponent<CharacterController>();
        _targetRotation = transform.rotation;
        _transposer =  cinemachineCamera.GetCinemachineComponent<CinemachineTransposer>();

        velocityHash = Animator.StringToHash("Speed");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FixedUpdate()
    {
        if (GameplayManager.Instance.canPlay)
        {
             //Create a sphere that check if an object of Ground Layer is beneath the player
        //isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        

        //Create a sphere that check if an object of Ground Layer is beneath the player
        isOnIce = Physics.CheckSphere(groundCheck.position, checkDistance, iceLayer);

        
        //Version TPS 
        Vector3 move1 = new Vector3(0f,0f,vertical);
        
        //Enable a smooth rotation of the Y axis relateted to the horizontal axis
        if (horizontal != 0) 
        { 
            _targetRotation *=  Quaternion.AngleAxis(rotationSensivity, new Vector3(0f,horizontal,0f));
        }

        transform.rotation= Quaternion.Lerp (transform.rotation, _targetRotation , 10 * Time.deltaTime);

        //Make the player move forward or backward
        if (move1.magnitude >= 0.1f)
        {
            timer = timerMax;
            if (vertical > 0)
            {
                //Forward move
                _charaController.Move(transform.forward*vertical  * (speed * Time.deltaTime));
            }
            else
            {
                //Backward move
                _charaController.Move(transform.forward*vertical  * (slopeSpeed * Time.deltaTime));
            }
            stockedVelocity = _charaController.velocity;
            sTimer -= Time.deltaTime;
            if (sTimer <= 0)
            {
                int randomSound = 0;
                randomSound = Random.Range(1, 2);
                AudioManager.audioManager.Play($"Footstep0{randomSound}");
                sTimer = soundTimer;
            }
        }
        else
        {
            if (isOnIce)
            {
                if (timer >= 0f )
                {
                    timer -= Time.deltaTime;
                    _charaController.Move(stockedVelocity * timer * 0.10f);
                }
                else
                {
                    _charaController.Move(Vector3.zero);
                }
            }
        }
        
        
        //Animations
        if (_animator)
        {
            _animator.SetFloat(velocityHash, vertical);
        }

        //Gravity Management for the Jumps
        if (_velocity.y < 0)
        {
            _velocity.y = -2f;
        }

        _velocity.y += gravity * Time.deltaTime * fallMultiplier;
        _charaController.Move(_velocity *Time.deltaTime);

        RaycastHit hit;

        if (Physics.Raycast(slopeDetector.position, slopeDetector.TransformDirection(Vector3.forward), out hit, 1.5f,layerTerrain))
        {
            Debug.DrawRay(slopeDetector.position, slopeDetector.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            speed = slopeSpeed;
            
            _animator.SetBool("isClimbing", true);
            
            //Modifying Follow Offset while climbing a bit
            if (_transposer)
            {
                _transposer.m_FollowOffset.y = Mathf.Lerp(_transposer.m_FollowOffset.y, 0, offsetSpeedMultiplier * Time.deltaTime);
            }
        }
        else
        {
            Debug.DrawRay(slopeDetector.position, slopeDetector.TransformDirection(Vector3.forward) * 1.5f, Color.white);
            speed = normalSpeed;
            
            _animator.SetBool("isClimbing", false);
            
            //Modifying Follow Offset while not climbing
            if (_transposer)
            {
                _transposer.m_FollowOffset.y = Mathf.Lerp(_transposer.m_FollowOffset.y, 5, offsetSpeedMultiplier * Time.deltaTime);
            }
        }
        }
       
    }

    public CinemachineVirtualCamera ChangeCam(CinemachineVirtualCamera camera)
    {
        camera.gameObject.SetActive(true);
        cinemachineCamera.Follow = null;
        cinemachineCamera.LookAt = null;
        CinemachineVirtualCamera cam = cinemachineCamera;
        //Reset the previous cam
        
        //Config the new one
        //camera.m_Priority = cinemachineCamera.Priority + 10;
        //camera.Follow = transform;
        //camera.LookAt = transform;

        cinemachineCamera = camera;
        _transposer = cinemachineCamera.GetComponent<CinemachineTransposer>();

        //Vector3.Lerp(_transposer.m_FollowOffset,offsetCam,Time.deltaTime*10f);
        //_transposer.m_FollowOffset = offsetCam;
        

        Debug.Log(cinemachineCamera);
        
        //Retturn the old cam
        return cam;
    }

    public void ActivateAnimPlayer()
    {
        _animator.SetTrigger("StartGame");
    }


    public void MovePlayer(Vector3 pos)
    {
        CharacterController characterController = GetComponent<CharacterController>();
        characterController.enabled = false;
        characterController.transform.position = pos;
        characterController.enabled = true;
    }
}
