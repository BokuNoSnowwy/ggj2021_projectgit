﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Lantern : MonoBehaviour
{
    public Light light;
    public float minRange, maxRange, minIntensity, maxIntensity, actualIntensity;
    [Range(1f, 10f)] public float intensityMultiplier;
    public float cooldown;

    public bool isFading;
    public bool isRefreshingLight;

    public bool isNearShadowBool;
    public float shadowMalus = 1;
    
    [SerializeField] private float decreasingLightSpeed; 

    public void Start()
    {
        StartCoroutine("LightRange");
        Debug.Log(light.intensity);
        actualIntensity = (light.intensity / 79.57747f) * intensityMultiplier;
        maxIntensity = light.intensity;
        RefreshLight();
    }

    // Update is called once per frame
    void Update()
    {

        light.intensity = actualIntensity * 79.57747f * intensityMultiplier;

        if (isNearShadowBool)
        {
            shadowMalus = 5f;
        }
        else
        {
            shadowMalus = 1f;
        }

        if (GameplayManager.Instance.canPlay)
        {
            if (isRefreshingLight)
            {
                if (actualIntensity < 100)
                {
                    actualIntensity += Time.deltaTime * 30f;
                }
                else
                {
                    isFading = true;
                    isRefreshingLight = false;
                }
        
            }
            else
            {
                if (isFading)
                {
                    actualIntensity -= Time.deltaTime * decreasingLightSpeed * shadowMalus;
                }
            }

            if (actualIntensity <= 0.5f)
            {
                Debug.Log("GameOver");

                if (GameplayManager.Instance)
                {
                    GameplayManager.Instance.RespawnPlayer();
                }
            }
        }
    }

    public void isNearShadow()
    {
        isNearShadowBool = !isNearShadowBool;
    }
    

    public IEnumerator LightRange()
    {
        while (light.intensity > minIntensity)
        {
            float range = Random.Range(minRange, maxRange);
            light.range = range;
            
            yield return new WaitForSeconds(cooldown);
        }

        yield return null;
    }

    public void RefreshLight()
    {
        actualIntensity = 100f;
    }

    public void RefreshingTheLight()
    {
        isFading = false;
        isRefreshingLight = true;
    }

    public void RespawnLantern()
    {
        RefreshLight();
        isRefreshingLight = false;
        isFading = true;
        isNearShadowBool = false;
    }
}
