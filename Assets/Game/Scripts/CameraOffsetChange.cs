﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraOffsetChange : MonoBehaviour
{
    public CinemachineVirtualCamera virtualCamera;
    public bool finalScene;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            virtualCamera = other.GetComponent<PlayerController>().ChangeCam(virtualCamera);
            virtualCamera.gameObject.SetActive(false);

            if (finalScene)
            {
                Debug.Log("Anim de fin");
                other.GetComponent<PlayerScript>().EndScene();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //throw new NotImplementedException();
    }
}
