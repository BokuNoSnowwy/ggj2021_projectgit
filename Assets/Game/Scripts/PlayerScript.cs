﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    [Header("CheckZone")] 
    [SerializeField] private bool onSafeZone;
    [SerializeField] private bool onDarkZone;

    public LayerMask safeZoneMask;

    [Header("DarkZone")]
    [SerializeField] private float darkValue;
    [SerializeField] [Range(0,1)] private float speedMultiplier;

    [SerializeField] private float speedDarkMultiplierIn;
    [SerializeField] private float speedDarkMultiplierOut;


    [Header("SnowFall")] 
    [SerializeField] private GameObject snowFall;
    
    
    private bool isDead;
    private GameplayManager _gameplayManager;

    [Header("Animation")] 
    [SerializeField] private Animator _animator;
    

    // Start is called before the first frame update
    void Start()
    {
        _gameplayManager = GameplayManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (snowFall)
        {
            snowFall.transform.position = transform.position;
        }
        
        
        if (!isDead && _gameplayManager.canPlay)
        {
            Vignette vignette;
            //Vignette actualisation
            _gameplayManager.volume.profile.TryGet(out vignette);
            //Debug.Log(100f-GetComponentInChildren<Lantern>().actualIntensity);
            vignette.intensity.value = ((100f-GetComponentInChildren<Lantern>().actualIntensity) * 0.5f * 1f / 100f);
            //Debug.Log(vignette.intensity.value); 
            
            if (onDarkZone)
            {
                if (darkValue <= 90)
                {
                    speedMultiplier = Mathf.Lerp(speedMultiplier,1f,Time.deltaTime);
                    darkValue += Time.deltaTime * speedDarkMultiplierIn;
                }
                else
                {
                    _gameplayManager.RespawnPlayer();
                    isDead = true;
                }
            }
            else
            {
                if (darkValue >= 0)
                {
                    speedMultiplier = Mathf.Lerp(speedMultiplier,0.1f,Time.deltaTime);
                    darkValue -= Time.deltaTime * speedDarkMultiplierOut;
                }
            }
        }
    }

    public void ResetPlayer()
    {
        isDead = false;
        onDarkZone = false;
        onSafeZone = true;
        darkValue = 1;
        GetComponentInChildren<Lantern>().RespawnLantern();
    }


    public void RefreshLight()
    {
        GetComponentInChildren<Lantern>().RefreshingTheLight();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Checkpoint"))
        {
            GameplayManager.Instance.commandPanel.transform.GetChild(1).GetComponent<Image>().DOFade(1f, 1f)
                .SetEase(Ease.Linear);
        }

        if (other.CompareTag("Shadow"))
        {
            GetComponentInChildren<Lantern>().isNearShadow();
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("DarkZone"))
        {
            onDarkZone = true;
        }
        
        if (other.gameObject.layer == LayerMask.NameToLayer("SafeZone"))
        {
            onSafeZone = true;
        }
    }
    

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Checkpoint"))
        {
            if (Input.GetButtonDown("Jump"))
            {
                //TODO Play Anim
                _animator.SetTrigger("Checkpoint");
                RefreshLight();
                _gameplayManager.ChangeSpawner(other.gameObject);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Checkpoint"))
        {
            GameplayManager.Instance.commandPanel.transform.GetChild(1).GetComponent<Image>().DOFade(0f, 1f)
                .SetEase(Ease.Linear);
            _animator.ResetTrigger("Checkpoint");
        }
        
        if (other.CompareTag("Shadow"))
        {
            GetComponentInChildren<Lantern>().isNearShadow();
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("SafeZone"))
        {
            Debug.Log("OutSafeZone, Pos : " + other.transform.position);
            onSafeZone = false; 
            //lastPointSafeZone = Instantiate(prefabTest, transform.position, Quaternion.identity);
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("DarkZone"))
        {
            Debug.Log("OutDarkZone, Pos : " + other.transform.position);
            onDarkZone = false;
        }
    }
    
    void OnDrawGizmos()
    {
        
    }

    public void EndScene()
    {
        _gameplayManager.EndGamePanelEvent();
    }
}
