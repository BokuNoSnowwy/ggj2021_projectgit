﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{

    public static GameplayManager Instance;

    [Header("RespawnPlayer")] 
    public GameObject respawnPoint;

    [Header("UI")] 
    public GameObject panelFadeIn_Out;
    public Volume volume;
    public GameObject pausePanel;
    public GameObject preGameMenu;
    public GameObject commandPanel;

    public GameObject endGamePanel;

    private bool onPause;

    [Header("Player")] 
    public GameObject player;

    public bool canPlay;

    [SerializeField] private float fadeInTime;

    [Header("Shadow")] 
    public GameObject shadowPrefab;
    public GameObject shadow;
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        if (AudioManager.audioManager)
        {
            AudioManager.audioManager.Play("BGM");
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Respawn"))
        {
            RespawnPlayer();
        }

        if (canPlay && Input.GetButtonDown("PauseButton"))
        {
            PauseGame();
        }
    }

    public void ChangeSpawner(GameObject checkpoint)
    {
        if (checkpoint != respawnPoint)
        {
            respawnPoint = checkpoint;
        }

        if (respawnPoint.GetComponent<Checkpoint>().shadowRespawnPoint != null)
        {
            MoveShadow();
            Debug.Log("spawnShadow");
        }

    }

    public void RespawnPlayer()
    {
        if (respawnPoint)
        {
            canPlay = false;
            Sequence sequenceFadeIn = DOTween.Sequence();
        
            Invoke("Respawn",fadeInTime);
            sequenceFadeIn.Append(panelFadeIn_Out.GetComponent<Image>().DOFade(1,fadeInTime).SetEase(Ease.Linear))
               // .Append(panelFadeIn_Out.GetComponent<Image>().DOFade(1,2f))
                .Append(panelFadeIn_Out.GetComponent<Image>().DOFade(0,1f).SetEase(Ease.InOutQuad));
        }
    }

    private void Respawn()
    {
        canPlay = true;
        player.GetComponent<PlayerController>().MovePlayer(respawnPoint.GetComponent<Checkpoint>().respawnPoint.transform.position);
        player.GetComponent<PlayerScript>().ResetPlayer();

        //TODO Reset la shadow qui te suis
        MoveShadow();
    }

    public void MoveShadow()
    {
        if (shadow)
        {
            shadow.transform.position = respawnPoint.GetComponent<Checkpoint>().shadowRespawnPoint.transform.position;
        }
        else
        {
            CreateShadow();
        }
    }

    public void PlayGame()
    {
        Debug.Log("PlayGame");
        preGameMenu.SetActive(false);
        commandPanel.SetActive(true);
        canPlay = true;
        player.GetComponent<PlayerController>().ActivateAnimPlayer();
        for (int i = 0; i < commandPanel.transform.childCount; i++)
        {
            StartCoroutine(DisplayImageForSeconds(commandPanel.transform.GetChild(i).gameObject, 3f));
        }
       
        //StartCoroutine(DisplayImageForSeconds(commandPanel.transform.GetChild(1).gameObject, 3f));

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        if (!onPause)
        {
            Time.timeScale = 0f;
            pausePanel.SetActive(true);
            onPause = true;
        }
        else
        {
            Debug.Log("Unpause");
            Time.timeScale = 1f;
            pausePanel.SetActive(false);
            onPause = false;
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    //TODO Trigger this fonction at the end of the final animation in playerScript
    public void EndGamePanelEvent()
    {
        canPlay = false;
        Invoke("ShowEndGamePanel",6f);
        
        panelFadeIn_Out.GetComponent<Image>().DOFade(1f, 6f).SetEase(Ease.InQuart);
        
        for (int i = 0; i < endGamePanel.transform.childCount; i++)
        {
            endGamePanel.transform.GetChild(i).GetComponent<Image>().DOFade(0, 0f);
        }
        endGamePanel.SetActive(true);
    }

    public void ShowEndGamePanel()
    {
        for (int i = 0; i < endGamePanel.transform.childCount; i++)
        {
            endGamePanel.transform.GetChild(i).GetComponent<Image>().DOFade(1, 1f);
        }
    }

    public void CreateShadow()
    {
        shadow = Instantiate(shadowPrefab, respawnPoint.GetComponent<Checkpoint>().shadowRespawnPoint);
        shadow.GetComponent<NavMeshAgent>().Warp(respawnPoint.GetComponent<Checkpoint>().shadowRespawnPoint.position);
        MoveShadow();
    }

    public IEnumerator DisplayImageForSeconds(GameObject gameobject, float seconds)
    {
        gameobject.GetComponent<Image>().DOFade(1, 0.01f);
        yield return new WaitForSeconds(seconds);
        gameobject.GetComponent<Image>().DOFade(0, 0.01f);
    }
}
